Selection set operations for Kakoune
====================================

Kakoune make it easy to work with multiple selections,
it lets you store a set of selections with `Z`
and restore them later with `z`.

You can also combine the current and stored selections
in various ways
through the `<a-z>` and `<a-Z>` keys.
Unfortunately,
the particular operations Kakoune provides are unusual,
and many people find them difficult to use.

This plugin provides alternative operations
for combining current and saved selections,
ones which will hopefully be easier to understand.

If you use this plugin,
you may also be interested in [kakoune-mark-show],
which highlights the selections currently saved
to the `"^` register (the default mark register).

[kakoune-mark-show]: https://gitlab.com/Screwtapello/kakoune-mark-show

Features
========

  - Combine current with saved selections,
    and select the result, or save it back to the source register
  - Works with selections saved to any register
  - Can be used interactively, or from scripts
  - Supported operations include: union, intersection

Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing

        :echo %val{runtime}/autoload

    inside Kakoune.
 3. Put a copy of `selection-sets.kak` and `selection-sets-helper.py` inside
    the new autoload directory,
    such as by checking out this git repository:

        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-selection-sets.git

Usage
=====

The easiest way to use it is to add the following mappings
to your kakrc:

```
map global user -docstring "combine register with selection" \
    Z ': selection-sets-prompt %val{register} %val{register}<ret>'
map global user -docstring "combine selection with register" \
    z ': selection-sets-prompt selection %val{register}<ret>'
```

Because of [a limitation in Kakoune][forward-register],
you can only use a register prefix
(for example, typing `"a` before the mapping to operate on the `a` register)
when these mappings are in the `normal` or `user` modes.
In other modes, including custom user modes,
you can hard-code a specific register,
but you can't use `%val{register}`.

[forward-register]: https://github.com/mawww/kakoune/issues/4192

TODO
====

  - More set operations!
      - left difference
      - right difference
      - any others?

Hacking
=======

To test the Python helper, you can run:

```
mypy --strict selection-sets-helper.py
python3 -m unittest selection-sets-helper.py
```
