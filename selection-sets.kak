declare-option -hidden str selection_sets_source %sh{dirname "$kak_source"}

define-command selection-sets-validate-register -hidden -params 1.. %{
    evaluate-commands %sh{
        if [ x"$kak_selection_desc" = x ]; then
            echo fail Must be run in window scope
            exit
        fi

        # A valid mark begins with a buffile@timestamp@mainsel head
        case "$2" in
            *@*@*)
                register_buffile=${2%%@*}
                if [ x"$kak_buffile" != x"$register_buffile" ]; then
                    echo fail cannot combine selections from different buffers
                    exit
                fi
                # After the head, there must be at least one selection
                if [ "$#" -lt 3 ]; then
                    echo fail register "$1" does not contain a selections desc
                fi
                ;;
            *)
                echo fail register "$1" does not contain a selections desc
                ;;
        esac
    }
}

define-command selection-sets-invoke-helper -hidden -params 4.. %{
    evaluate-commands %sh{
        dest=$1
        op=$2
        selections=$3
        shift 3
        mark="$*"

        "$kak_opt_selection_sets_source/selection-sets-helper.py" \
            "$dest" "$op" "$selections" "$mark"
    }
}

define-command selection-sets-apply -params 3 -menu \
    -docstring "
        selection-sets-apply <dest> <register> <op>

        Applies the <op> operation to the selection and the mark in <register>,
        then stores the result in <dest>.

        <dest>     The register where the result will be stored.
                   If set to the keyword 'selection', the result will be
                   selected instead.
        <register> The register containing the mark to be combined with the
                   selection.
        <op>       A set operation: union, intersection
    " \
    -shell-script-candidates %{
        registers="a b c d e f g h i j k l m n o p q r s t u v w x y z ^"
        case "$kak_token_to_complete" in
            0)
                printf "%s\n" selection $registers
                ;;
            1)
                printf "%s\n" $registers
                ;;
            2)
                printf "%s\n" union intersection
                ;;
        esac
    } \
%{
    # Validate we're running in a sensible scope with a sensible register
    evaluate-commands "selection-sets-validate-register %%arg{2} %%reg{%arg{2}}"

    evaluate-commands %sh{
        echo selection-sets-invoke-helper \
            "$1" "$3" "'$kak_selections_desc'" "%reg{$2}"
    }

}

define-command selection-sets-prompt-inner -params 2 -hidden %{
    # Validate we're running in a sensible scope with a sensible register
    evaluate-commands "selection-sets-validate-register %%arg{2} %%reg{%arg{2}}"

    info -title "%arg{1} = ""%arg{2} ? sel" \
"u: union
i: intersection"

    on-key %{
        info # clear the info box
        evaluate-commands %sh{
            case "$kak_key" in
                u)
                    echo selection-sets-apply "'$1'" "'$2' union"
                    ;;
                i)
                    echo selection-sets-apply "'$1'" "'$2' intersection"
                    ;;
                *)
                    echo fail "no such combine operator $kak_key"
                    exit
                    ;;
            esac
        }
    }
}

# FIXME: This could be replaced with a custom user mode
# if https://github.com/mawww/kakoune/issues/4192 gets fixed.
#
# We wouldn't even need a user mode
# if https://github.com/mawww/kakoune/issues/3572 gets fixed.
define-command selection-sets-prompt -params 2 -menu \
    -docstring "
        selection-sets-prompt <dest> <register>

        Displays a prompt asking how to combine the current selection
        with the given register.

        <dest>     The register where the result will be stored.
                   If set to the keyword 'selection', the result will be
                   selected instead.
        <register> The register containing the mark operand.
    " \
    -shell-script-candidates %{
        registers="a b c d e f g h i j k l m n o p q r s t u v w x y z ^"
        case "$kak_token_to_complete" in
            0)
                printf "%s\n" selection $registers
                ;;
            1)
                printf "%s\n" $registers
                ;;
        esac
    } \
%{
    # Call selection-sets-prompt-inner, defaulting to the ^ register
    evaluate-commands %sh{
        echo selection-sets-prompt-inner "${1:-^}" "${2:-^}"
    }
}
