#!/usr/bin/env python3
from dataclasses import dataclass
import sys
from typing import Sequence, Tuple, List, Iterable, overload, Any, Optional
import unittest


@dataclass(order=True, frozen=True)
class Coord:
    line: int
    column: int

    @classmethod
    def from_kakoune(cls, text: str) -> "Coord":
        (line, column) = text.split(".")
        return cls(int(line), int(column))

    def to_kakoune(self) -> str:
        return f"{self.line}.{self.column}"

    def __add__(self, other: "Coord") -> "Coord":
        return Coord(self.line + other.line, self.column + other.column)

    def __sub__(self, other: "Coord") -> "Coord":
        return Coord(self.line - other.line, self.column - other.column)

    def __abs__(self) -> "Coord":
        return Coord(abs(self.line), abs(self.column))


@dataclass(order=True, frozen=True)
class Span:
    anchor: Coord
    cursor: Coord

    @classmethod
    def from_kakoune(cls, text: str) -> "Span":
        (raw_anchor, raw_cursor) = text.split(",")
        anchor = Coord.from_kakoune(raw_anchor)
        cursor = Coord.from_kakoune(raw_cursor)

        return cls(anchor, cursor)

    def to_kakoune(self) -> str:
        anchor = self.anchor.to_kakoune()
        cursor = self.cursor.to_kakoune()
        return f"{anchor},{cursor}"

    @property
    def start(self) -> Coord:
        if self.anchor < self.cursor:
            return self.anchor
        else:
            return self.cursor

    @property
    def end(self) -> Coord:
        if self.anchor > self.cursor:
            return self.anchor
        else:
            return self.cursor

    def __contains__(self, item: Coord) -> bool:
        return self.start <= item <= self.end


class Selections(Sequence[Span]):
    _items: Sequence[Span]

    def __init__(self, items: Iterable[Span]) -> None:
        self._items = tuple(sorted(items))

    @classmethod
    def from_selections_desc(cls, text: str) -> Tuple[Coord, "Selections"]:
        selections = [Span.from_kakoune(each) for each in text.split()]
        main_cursor = selections[0].cursor
        return (main_cursor, cls(selections))

    @classmethod
    def from_mark(cls, text: str) -> Tuple[str, str, Coord, "Selections"]:
        (head, *raw_selections) = text.split()
        (buffile, timestamp, main_cursor_index) = head.split("@")
        selections = cls(Span.from_kakoune(each) for each in raw_selections)
        main_cursor = selections[int(main_cursor_index)].cursor

        return buffile, timestamp, main_cursor, selections

    def nearest_cursor_index(self, target: Coord) -> Optional[int]:
        if len(self._items) == 0:
            return None

        nearest_distance = abs(self._items[0].cursor - target)
        nearest_index = 0
        for index, span in enumerate(self._items):
            distance = abs(span.cursor - target)
            if distance < nearest_distance:
                nearest_distance = distance
                nearest_index = index

        return nearest_index

    def union(self, other: "Selections") -> "Selections":
        these_spans = iter(self)
        those_spans = iter(other)
        new_spans: List[Span] = []

        this: Optional[Span] = next(these_spans, None)
        that: Optional[Span] = next(those_spans, None)

        while this is not None and that is not None:
            # If one span is wholly before the other,
            # use it as-is.
            if this.end < that.start:
                new_spans.append(this)
                this = next(these_spans, None)

            elif that.end < this.start:
                new_spans.append(that)
                that = next(those_spans, None)

            # These spans overlap, so combine them.
            else:
                new_spans.append(
                    Span(min(this.start, that.start), max(this.end, that.end))
                )
                this = next(these_spans, None)
                that = next(those_spans, None)

        if this is None and that is not None:
            new_spans.append(that)
            new_spans.extend(those_spans)
        elif this is not None and that is None:
            new_spans.append(this)
            new_spans.extend(these_spans)

        return Selections(new_spans)

    def intersection(self, other: "Selections") -> "Selections":
        these_spans = iter(self)
        those_spans = iter(other)
        new_spans: List[Span] = []

        this: Optional[Span] = next(these_spans, None)
        that: Optional[Span] = next(those_spans, None)

        while this is not None and that is not None:
            # If one span is wholly before the other,
            # it does not intersect.
            if this.end < that.start:
                this = next(these_spans, None)

            elif that.end < this.start:
                that = next(those_spans, None)

            # These spans overlap, so intersect them.
            else:
                new_spans.append(
                    Span(max(this.start, that.start), min(this.end, that.end))
                )
                this = next(these_spans, None)
                that = next(those_spans, None)

        return Selections(new_spans)

    def to_select_command(self, main_span_index: Optional[int]) -> str:
        if len(self._items) == 0:
            return "fail no selections remaining"
        if main_span_index is None:
            main_span_index = 0

        res = ["select"]
        res.extend(each.to_kakoune() for each in self._items[main_span_index:])
        res.extend(each.to_kakoune() for each in self._items[:main_span_index])
        return " ".join(res)

    def to_register_command(
        self,
        register: str,
        bufname: str,
        timestamp: str,
        main_span_index: Optional[int],
    ) -> str:
        if len(self._items) == 0:
            return "fail no selections remaining"
        if main_span_index is None:
            main_span_index = 0

        res = [
            "set-register",
            register,
            f"{bufname}@{timestamp}@{main_span_index}",
        ]
        res.extend(each.to_kakoune() for each in self._items)
        return " ".join(res)

    def __len__(self) -> int:
        return len(self._items)

    @overload
    def __getitem__(self, i: int) -> Span:
        ...

    @overload
    def __getitem__(self, s: slice) -> Sequence[Span]:
        ...

    def __getitem__(self, index: Any) -> Any:
        return self._items[index]


def build_kakoune_command(
    dest: str,
    op: str,
    selections_main_cursor: Coord,
    selections: Selections,
    buffile: str,
    timestamp: str,
    mark_main_cursor: Coord,
    mark: Selections,
) -> str:

    if op == "union":
        result = selections.union(mark)
    elif op == "intersection":
        result = selections.intersection(mark)
    else:
        raise ValueError(f"Unrecognised operation {op!r}")

    if dest == "selection":
        main_cursor_index = result.nearest_cursor_index(selections_main_cursor)
        return result.to_select_command(main_cursor_index)
    elif len(dest) == 1:
        main_cursor_index = result.nearest_cursor_index(mark_main_cursor)
        return result.to_register_command(
            dest, buffile, timestamp, main_cursor_index
        )
    else:
        raise ValueError(f"Unrecognised destination {dest!r}")


def main(
    dest: str,
    op: str,
    raw_selections: str,
    raw_mark: str,
) -> int:
    selections_main_cursor, selections = Selections.from_selections_desc(
        raw_selections
    )
    buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(raw_mark)

    print(
        build_kakoune_command(
            dest,
            op,
            selections_main_cursor,
            selections,
            buffile,
            timestamp,
            mark_main_cursor,
            mark,
        )
    )

    return 0


if __name__ == "__main__":
    sys.exit(main(*sys.argv[1:]))


class TestCoord(unittest.TestCase):
    def test_from_kakoune(self) -> None:
        coord = Coord.from_kakoune("1.2")
        self.assertEqual(coord.line, 1)
        self.assertEqual(coord.column, 2)

    def test_to_kakoune(self) -> None:
        expected = "12.34"
        actual = Coord.from_kakoune(expected).to_kakoune()
        self.assertEqual(actual, expected)

    def test_subtract_identical(self) -> None:
        coord = Coord(10, 20)
        zero = coord - coord

        # coord is unmodified
        self.assertEqual(coord.line, 10)
        self.assertEqual(coord.column, 20)

        # zero is actually zero
        self.assertEqual(zero.line, 0)
        self.assertEqual(zero.column, 0)

    def test_subtract_lesser(self) -> None:
        x = Coord(7, 8)
        y = Coord(5, 6)
        diff = x - y

        self.assertEqual(diff.line, 2)
        self.assertEqual(diff.column, 2)

    def test_subtract_greater(self) -> None:
        x = Coord(5, 6)
        y = Coord(7, 8)
        diff = x - y

        self.assertEqual(diff.line, -2)
        self.assertEqual(diff.column, -2)

    def test_add_zero(self) -> None:
        coord = Coord(10, 20)
        zero = Coord(0, 0)

        self.assertEqual(coord + zero, coord)

    def test_add_line_delta(self) -> None:
        coord = Coord(5, 6)
        delta = Coord(2, 2)
        sum = Coord(7, 8)

        self.assertEqual(coord + delta, sum)
        self.assertEqual(delta + coord, sum)

    def test_abs(self) -> None:
        self.assertEqual(abs(Coord(1, 2)), Coord(1, 2))
        self.assertEqual(abs(Coord(-1, 2)), Coord(1, 2))
        self.assertEqual(abs(Coord(1, -2)), Coord(1, 2))
        self.assertEqual(abs(Coord(-1, -2)), Coord(1, 2))


class TestSpan(unittest.TestCase):
    def test_from_kakoune(self) -> None:
        span = Span.from_kakoune("1.2,3.4")
        self.assertEqual(span.anchor.line, 1)
        self.assertEqual(span.anchor.column, 2)
        self.assertEqual(span.cursor.line, 3)
        self.assertEqual(span.cursor.column, 4)

    def test_to_kakoune(self) -> None:
        expected = "12.34,56.78"
        actual = Span.from_kakoune(expected).to_kakoune()
        self.assertEqual(actual, expected)

    def test_forward_block(self) -> None:
        span = Span.from_kakoune("1.2,3.4")
        self.assertEqual(span.start.line, 1)
        self.assertEqual(span.start.column, 2)
        self.assertEqual(span.end.line, 3)
        self.assertEqual(span.end.column, 4)

    def test_backward_block(self) -> None:
        span = Span.from_kakoune("3.4,1.2")
        self.assertEqual(span.start.line, 1)
        self.assertEqual(span.start.column, 2)
        self.assertEqual(span.end.line, 3)
        self.assertEqual(span.end.column, 4)

    def test_forward_word(self) -> None:
        span = Span.from_kakoune("1.2,1.3")
        self.assertEqual(span.start.line, 1)
        self.assertEqual(span.start.column, 2)
        self.assertEqual(span.end.line, 1)
        self.assertEqual(span.end.column, 3)

    def test_backward_word(self) -> None:
        span = Span.from_kakoune("1.3,1.2")
        self.assertEqual(span.start.line, 1)
        self.assertEqual(span.start.column, 2)
        self.assertEqual(span.end.line, 1)
        self.assertEqual(span.end.column, 3)


class TestSelections(unittest.TestCase):
    def test_from_selections_desc(self) -> None:
        main_cursor, selections = Selections.from_selections_desc(
            "2.1,2.5 3.1,3.5 1.1,1.5"
        )
        self.assertEqual(main_cursor, Coord.from_kakoune("2.5"))
        self.assertEqual(
            list(selections),
            [
                Span.from_kakoune("1.1,1.5"),
                Span.from_kakoune("2.1,2.5"),
                Span.from_kakoune("3.1,3.5"),
            ],
        )

    def test_from_mark(self) -> None:
        buffile, timestamp, main_cursor, selections = Selections.from_mark(
            "/test.txt@5678@1 1.1,1.5 2.1,2.5 3.1,3.5"
        )
        self.assertEqual(buffile, "/test.txt")
        self.assertEqual(timestamp, "5678")
        self.assertEqual(main_cursor, Coord(2, 5))
        self.assertEqual(
            list(selections),
            [
                Span.from_kakoune("1.1,1.5"),
                Span.from_kakoune("2.1,2.5"),
                Span.from_kakoune("3.1,3.5"),
            ],
        )

    def test_nearest_cursor_index(self) -> None:
        _, selections = Selections.from_selections_desc(
            "1.1,1.2 4.4,4.8 4.9,4.12"
        )

        self.assertEqual(selections.nearest_cursor_index(Coord(1, 1)), 0)
        self.assertEqual(selections.nearest_cursor_index(Coord(1, 5)), 0)
        self.assertEqual(selections.nearest_cursor_index(Coord(1, 10)), 0)
        self.assertEqual(selections.nearest_cursor_index(Coord(2, 1)), 0)
        self.assertEqual(selections.nearest_cursor_index(Coord(3, 1)), 1)
        self.assertEqual(selections.nearest_cursor_index(Coord(4, 1)), 1)
        self.assertEqual(selections.nearest_cursor_index(Coord(4, 7)), 1)
        self.assertEqual(selections.nearest_cursor_index(Coord(4, 8)), 1)
        self.assertEqual(selections.nearest_cursor_index(Coord(4, 9)), 1)
        self.assertEqual(selections.nearest_cursor_index(Coord(4, 10)), 1)
        self.assertEqual(selections.nearest_cursor_index(Coord(4, 11)), 2)

        self.assertEqual(Selections([]).nearest_cursor_index(Coord(0, 0)), None)

    def test_union(self) -> None:
        _, a = Selections.from_selections_desc("2.1,2.7 1.1,1.10")
        _, b = Selections.from_selections_desc("2.2,2.10 3.1,3.10")
        result = a.union(b)

        self.assertEqual(
            list(result),
            [
                Span.from_kakoune("1.1,1.10"),
                Span.from_kakoune("2.1,2.10"),
                Span.from_kakoune("3.1,3.10"),
            ],
        )

    def test_unequal_unions(self) -> None:
        _, a = Selections.from_selections_desc("1.1,1.1")
        _, b = Selections.from_selections_desc("2.1,2.1 3.1,3.1")
        result = a.union(b)

        self.assertEqual(
            list(result),
            [
                Span.from_kakoune("1.1,1.1"),
                Span.from_kakoune("2.1,2.1"),
                Span.from_kakoune("3.1,3.1"),
            ],
        )

        result = b.union(a)

        self.assertEqual(
            list(result),
            [
                Span.from_kakoune("1.1,1.1"),
                Span.from_kakoune("2.1,2.1"),
                Span.from_kakoune("3.1,3.1"),
            ],
        )

    def test_to_select_command(self) -> None:
        _, selections = Selections.from_selections_desc(
            "1.1,1.5 2.1,2.5 3.1,3.5"
        )

        self.assertEqual(
            selections.to_select_command(None),
            "select 1.1,1.5 2.1,2.5 3.1,3.5",
        )
        self.assertEqual(
            selections.to_select_command(0),
            "select 1.1,1.5 2.1,2.5 3.1,3.5",
        )
        self.assertEqual(
            selections.to_select_command(1),
            "select 2.1,2.5 3.1,3.5 1.1,1.5",
        )
        self.assertEqual(
            selections.to_select_command(2),
            "select 3.1,3.5 1.1,1.5 2.1,2.5",
        )
        self.assertEqual(
            Selections([]).to_select_command(0),
            "fail no selections remaining",
        )

    def test_to_register_command(self) -> None:
        _, selections = Selections.from_selections_desc(
            "1.1,1.5 2.1,2.5 3.1,3.5"
        )

        self.assertEqual(
            selections.to_register_command("^", "/test.txt", "5678", None),
            "set-register ^ /test.txt@5678@0 1.1,1.5 2.1,2.5 3.1,3.5",
        )
        self.assertEqual(
            selections.to_register_command("^", "/test.txt", "5678", 0),
            "set-register ^ /test.txt@5678@0 1.1,1.5 2.1,2.5 3.1,3.5",
        )
        self.assertEqual(
            selections.to_register_command("^", "/test.txt", "5678", 1),
            "set-register ^ /test.txt@5678@1 1.1,1.5 2.1,2.5 3.1,3.5",
        )
        self.assertEqual(
            selections.to_register_command("^", "/test.txt", "5678", 2),
            "set-register ^ /test.txt@5678@2 1.1,1.5 2.1,2.5 3.1,3.5",
        )
        self.assertEqual(
            Selections([]).to_register_command("^", "/test.txt", "5678", 0),
            "fail no selections remaining",
        )


class TestBuildKakouneCommand(unittest.TestCase):
    def test_union_to_selection(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "2.1,2.7 1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@1 2.2,2.10 3.1,3.10"
        )
        result = build_kakoune_command(
            "selection",
            "union",
            selections_main_cursor,
            selections,
            buffile,
            timestamp,
            mark_main_cursor,
            mark,
        )

        self.assertEqual(result, "select 2.1,2.10 3.1,3.10 1.1,1.10")

    def test_union_to_register(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "2.1,2.7 1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@0 2.2,2.10 3.1,3.10"
        )
        result = build_kakoune_command(
            "^",
            "union",
            selections_main_cursor,
            selections,
            buffile,
            timestamp,
            mark_main_cursor,
            mark,
        )

        self.assertEqual(
            result,
            "set-register ^ /test.txt@5678@1 1.1,1.10 2.1,2.10 3.1,3.10",
        )

    def test_intersection_to_selection(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "2.1,2.7 1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@1 2.2,2.10 3.1,3.10"
        )
        result = build_kakoune_command(
            "selection",
            "intersection",
            selections_main_cursor,
            selections,
            buffile,
            timestamp,
            mark_main_cursor,
            mark,
        )

        self.assertEqual(result, "select 2.2,2.7")

    def test_intersection_to_register(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "2.1,2.7 1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@0 2.2,2.10 3.1,3.10"
        )
        result = build_kakoune_command(
            "^",
            "intersection",
            selections_main_cursor,
            selections,
            buffile,
            timestamp,
            mark_main_cursor,
            mark,
        )

        self.assertEqual(
            result,
            "set-register ^ /test.txt@5678@0 2.2,2.7",
        )

    def test_empty_intersection_to_selection(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@0 3.1,3.10"
        )
        result = build_kakoune_command(
            "selection",
            "intersection",
            selections_main_cursor,
            selections,
            buffile,
            timestamp,
            mark_main_cursor,
            mark,
        )

        self.assertEqual(result, "fail no selections remaining")

    def test_empty_intersection_to_register(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@0 3.1,3.10"
        )
        result = build_kakoune_command(
            "^",
            "intersection",
            selections_main_cursor,
            selections,
            buffile,
            timestamp,
            mark_main_cursor,
            mark,
        )

        self.assertEqual(result, "fail no selections remaining")

    def test_to_unknown_dest(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "2.1,2.7 1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@1 2.2,2.10 3.1,3.10"
        )
        with self.assertRaises(ValueError) as catcher:
            build_kakoune_command(
                "blorp",
                "union",
                selections_main_cursor,
                selections,
                buffile,
                timestamp,
                mark_main_cursor,
                mark,
            )

        self.assertEqual(
            str(catcher.exception), "Unrecognised destination 'blorp'"
        )

    def test_unknown_op(self) -> None:
        selections_main_cursor, selections = Selections.from_selections_desc(
            "2.1,2.7 1.1,1.10"
        )
        buffile, timestamp, mark_main_cursor, mark = Selections.from_mark(
            "/test.txt@5678@1 2.2,2.10 3.1,3.10"
        )
        with self.assertRaises(ValueError) as catcher:
            build_kakoune_command(
                "selection",
                "blorp",
                selections_main_cursor,
                selections,
                buffile,
                timestamp,
                mark_main_cursor,
                mark,
            )

        self.assertEqual(
            str(catcher.exception), "Unrecognised operation 'blorp'"
        )
